## Requirements

Installed [Composer](https://getcomposer.org) Dependency Manager for PHP

## Setup

```bash
composer update
```
