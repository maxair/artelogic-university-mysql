<?php require_once "includes/db_connection.php";

/**
 * Prepares and executes a SQL statement
 *
 * @param string $sql SQL statement
 * @param array|null $args Arguments passed to the SQL statement
 * @return PDOStatement PDOStatement object
 * @throws PDOException If the database server cannot successfully prepare the statement
 */
function run(string $sql, array $args = null): PDOStatement {
    global $pdo;
    $stmt = $pdo->prepare($sql);
    $stmt->execute($args);

    return $stmt;
}

/**
 * Calculates average students availability per current month for all classes
 *
 * @return int|false Average availability if there is a result, FALSE otherwise
 */
function getAvgAvailabilityPerCurrentMonth() {
    $sql = "SELECT 
                ROUND(AVG(is_available) * 100)
            FROM 
                 availabilities
            WHERE 
                availability_date BETWEEN (LAST_DAY(CURDATE()) + INTERVAL 1 DAY - INTERVAL 1 MONTH) 
                    AND (LAST_DAY(CURDATE()));";

    $stmt = run($sql);
    $result = $stmt->fetchColumn();

    return !is_null($result) ? (int) $result : false;
}

/**
 * Calculates average students availability per current month for a specific class
 *
 * @param string $class_name Class name
 * @return int|false Average availability if there is a result, FALSE otherwise
 */
function getAvgAvailabilityPerCurrentMonthByClass(string $class_name) {
    $sql = "SELECT 
                ROUND(AVG(is_available) * 100)
            FROM
                availabilities a
            JOIN
                students s ON a.student_id = s.id
            JOIN
                classes c ON s.class_id = c.id
            WHERE
                class_name = ?
                    AND availability_date BETWEEN (LAST_DAY(CURDATE()) + INTERVAL 1 DAY - INTERVAL 1 MONTH) 
                    AND (LAST_DAY(CURDATE()));";

    $stmt = run($sql, [$class_name]);
    $result = $stmt->fetchColumn();

    return !is_null($result) ? (int) $result : false;
}

/**
 * Calculates average students availability per period
 *
 * @param string $start_date Start date
 * @param string $end_date End date
 * @return int|false Average availability if there is a result, FALSE otherwise
 */
function getAvgAvailabilityPerPeriod(string $start_date, string $end_date) {
    $sql = "SELECT 
                ROUND(AVG(is_available) * 100)
            FROM
                availabilities
            WHERE
                availability_date BETWEEN ? AND ?;";

    $stmt = run($sql, [$start_date, $end_date]);
    $result = $stmt->fetchColumn();

    return !is_null($result) ? (int) $result : false;
}

/**
 * Calculates the top N students by availability for each class
 *
 * @param int $limit Limit (e.g. top 3 students)
 * @return array|false An empty array is returned if there are zero results to fetch, or FALSE on failure
 */
function getTopStudentsByAvailabilityForEachClass(int $limit) {
    $sql = "SELECT 
                *
            FROM
	            (SELECT 
		            class_name, 
                    CONCAT(first_name, ' ', last_name) AS student,
                    availability_total,
                    DENSE_RANK() OVER (PARTITION BY class_name ORDER BY availability_total DESC) AS top
	            FROM
		            (SELECT 
			            class_name,
		                first_name,
                        last_name,
			            SUM(is_available) AS availability_total
		            FROM
			            availabilities a
		            JOIN 
			            students s ON a.student_id = s.id
		            JOIN 
			            classes c ON s.class_id = c.id
		            GROUP BY a.student_id
		            ) AS availability_sum
	            ) AS availability_top
            WHERE top <= ?;";

    $stmt = run($sql, [$limit]);

    return $stmt->fetchAll();
}

/**
 * Calculates the worst N students by the availability in the school
 *
 * @param int $limit Limit (e.g. worst 3 students)
 * @return array|false An empty array is returned if there are zero results to fetch, or FALSE on failure
 */
function getWorstStudentsByAvailability(int $limit) {
    $sql = "SELECT 
                *
            FROM
	            (SELECT 
	                class_name,
                    CONCAT(first_name, ' ', last_name) AS student, 
                    availability_total,
                    DENSE_RANK() OVER (ORDER BY availability_total) AS worst
	            FROM
		            (SELECT 
		                first_name,
                        last_name,
                        class_name,
			            SUM(is_available) AS availability_total
		            FROM
			            availabilities a
		            JOIN 
			            students s ON a.student_id = s.id
		            JOIN 
			            classes c ON s.class_id = c.id
		            GROUP BY a.student_id
		            ) AS availability_sum
	            ) AS availability_worst
            WHERE worst <= ?;";

    $stmt = run($sql, [$limit]);

    return $stmt->fetchAll();
}

/**
 * Displays tabular data
 *
 * @param array $data Multidimensional associative array
 * @return void
 */
function printTable(array $data): void {
    $table = new Console_Table();
    $headers = false;

    foreach ($data as $row) {
        if (!$headers) {
            $table->setHeaders(array_keys($row));
            $headers = true;
        }
        $table->addRow(array_values($row));
    }

    echo $table->getTable();
}

/**
 * Determines if date string is a valid date
 *
 * @param string $date Date
 * @param string $format Date format
 * @return bool TRUE if date string is a valid date in that format, false otherwise
 */
function isValidDate(string $date, string $format = "Y-m-d"): bool {
    $d = DateTime::createFromFormat($format, $date);

    return $d && $d->format($format) == $date;
}

/**
 * Handles all uncaught exceptions
 *
 * @param Exception $exception Exception object
 * @return void
 */
function exceptionHandler(Exception $exception): void {
    echo "Fail: " . $exception->getMessage() . PHP_EOL;
}