<?php


class Config
{
    /**
     * Database configuration
     */
    const DB_HOST = "localhost";
    const DB_PORT = "3307";
    const DB_NAME = "availability";
    const DB_USER = "root";
    const DB_PASSWORD = "str0ngp4ssw0rd";

    const DB_OPTIONS = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false
    ];
}