<?php require_once "functions.php";

/**
 * Composer autoloader
 */
require_once __DIR__ . "/vendor/autoload.php";

/**
 * Exception handler
 */
set_exception_handler("exceptionHandler");

/**
 * Main execution
 */

echo "Select one option below:" . PHP_EOL;
echo "1 - Calculate AVG students availability per current month for all classes" . PHP_EOL;
echo "2 - Calculate AVG students availability per current month for a specific class" . PHP_EOL;
echo "3 - Calculate AVG students availability per period" . PHP_EOL;
echo "4 - Calculate the top-N students by availability for each class" . PHP_EOL;
echo "5 - Calculate the worst-N students by the availability in the school" . PHP_EOL;
echo "Provide option number: ";

$selected_option = readline();

switch ($selected_option) {
    case 1:
        $result = getAvgAvailabilityPerCurrentMonth();
        if ($result !== false) {
            echo $result . "%" . PHP_EOL;
        }
        break;
    case 2:
        echo "Class name: ";
        $class_name = readline();
        $result = getAvgAvailabilityPerCurrentMonthByClass($class_name);
        if ($result !== false) {
            echo $result . "%" . PHP_EOL;
        }
        break;
    case 3:
        echo "Start date: ";
        $start_date = readline();
        echo "End date: ";
        $end_date = readline();
        if (isValidDate($start_date) && isValidDate($end_date)) {
            $result = getAvgAvailabilityPerPeriod($start_date, $end_date);
            if ($result !== false) {
                echo  $result. "%" . PHP_EOL;
            }
        } else {
            echo "Please, provide a valid dates (yyyy-mm-dd)";
        }
        break;
    case 4:
        echo "Limit: ";
        $limit = readline();
        $result = getTopStudentsByAvailabilityForEachClass((int) $limit);
        if ($result) {
            printTable($result);
        }
        break;
    case 5:
        echo "Limit: ";
        $limit = readline();
        $result = getWorstStudentsByAvailability((int) $limit);
        if ($result) {
            printTable($result);
        }
        break;
    default :
        echo "Please, select from list";
}
