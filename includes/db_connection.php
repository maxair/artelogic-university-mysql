<?php require_once "Config.php";

$dsn = "mysql:host=" . Config::DB_HOST . ";port=". Config::DB_PORT . ";dbname=" . Config::DB_NAME;

try{
    $pdo = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD, Config::DB_OPTIONS);
} catch (PDOException $exception){
    echo 'Database connection failed: ' . $exception->getMessage();
    exit();
}